import os

from minio import Minio
from minio.error import ResponseError

import utils

MINIO_ENDPOINT = os.getenv('MINIO_ENDPOINT', 'localhost:9001')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY', '970JMYRRHPDDR3VILELB')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY',
                             'D4eo0egGu2T5PAfw+BOuKmtvKGbPCXEIC3olWT66')

MINIO_CLIENT = Minio(
    MINIO_ENDPOINT,
    access_key=MINIO_ACCESS_KEY,
    secret_key=MINIO_SECRET_KEY,
    secure=False)


def is_bucket_exist(bucketname):
    utils.eprint('checking bucket exist')
    try:
        return MINIO_CLIENT.bucket_exists(bucketname)
    except ResponseError as err:
        utils.eprint(err)
        return False


def create_bucket(bucketname):
    utils.eprint('creating bucket: ' + bucketname)
    if not is_bucket_exist(bucketname):
        try:
            MINIO_CLIENT.make_bucket(bucketname)
            return "success"
        except ResponseError as err:
            utils.eprint(err)
    return "failure"


def upload(file, bucketname, objectname):
    create_bucket(bucketname)
    utils.eprint('uploading ' + bucketname + ":" + objectname + " to storage")
    try:
        with open(file, 'rb') as file_data:
            file_stat = os.stat(file)
            MINIO_CLIENT.put_object(bucketname, objectname, file_data,
                                    file_stat.st_size)
        return "success"
    except ResponseError as err:
        utils.eprint(err)


def download(bucketname, filename):
    utils.eprint('downloading ' + bucketname + ":" + filename +
                 " from storage")
    try:
        return MINIO_CLIENT.get_object(bucketname, filename)
    except ResponseError as err:
        utils.eprint(err)
