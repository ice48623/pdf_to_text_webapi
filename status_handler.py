import redis_services as rs


def handle_uploaded(bucket):
    rs.handle_uploaded(bucket)


def handle_extracted(bucket, total, targets):
    rs.handle_extracted(bucket, total, targets)


def handle_processed(bucket):
    rs.handle_processed(bucket)


def handle_compressed(bucket):
    rs.handle_compressed(bucket)


def handle_complete(bucket, object_name):
    rs.handle_complete(bucket, object_name)


def handle_error(bucket, error):
    rs.handle_error(bucket, error)
