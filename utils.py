import sys
import json

import redis_services as rs

PROGRESS_MAP = {'uploaded': 10, 'extracted': 20, 'processed': 60, 'compressed': 80, 'complete': 100}

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def to_string(string):
    if isinstance(string, bytes):
        return string.decode("utf-8")
    return string

def get_progress(message):
    return PROGRESS_MAP.get(message, 0)

def make_extract_message(bucket, filename):
    message = {
        'bucket': bucket,
        'object': filename
    }
    return json.dumps(message)

def make_upload_message(bucket, status):
    message = {
        'bucket': bucket,
        'status': status,
    }
    return json.dumps(message)

def get_bucket_name(filename):
    return filename.split('.')[0]

def parser(body):
    body = body.decode('utf8').replace("'", '"')
    return json.loads(body)

def make_bucket_key(room, bucketname):
    return bucketname

def convert_key_to_regis_key(key):
    room = to_string(rs.get_redis().get(f"{key}:room"))
    return f"{room}:{key}"
