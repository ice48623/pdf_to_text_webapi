import json
import os
import threading

import pika
import socketio
from flask import Flask, request, send_file
from flask_cors import CORS
from kombu import Connection, Consumer, Exchange, Queue
from werkzeug.utils import secure_filename

import minio_services as ms
import redis_services as rs
import status_handler as sh
import utils

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['tgz', 'tar.gz'])
RABBIT_HOST = os.getenv('RABBIT_HOST', 'localhost')
RABBIT_PORT = '5672'

APP = Flask(__name__)
APP.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
SIO = socketio.Server(async_mode='threading')
APP.wsgi_app = socketio.Middleware(SIO, APP.wsgi_app)
CORS(APP)


# Create place holder for uploaded files
def create_upload_dir():
    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)


create_upload_dir()


def allowed_file(filename):
    return '.' in filename and filename.split(
        '.', 1)[1].lower() in ALLOWED_EXTENSIONS


# Rabbit Sender


def update_status_upload(bucketname, status):
    message = utils.make_upload_message(bucketname, status)
    send_job('update_status', message)


def send_extract_job(bucket, filename):
    message = utils.make_extract_message(bucket, filename)
    send_job('extract', message)


def send_job(queue_name, message):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBIT_HOST, port=RABBIT_PORT))
    channel = connection.channel()

    channel.queue_declare(queue=queue_name, durable=True)

    channel.basic_publish(exchange='', routing_key=queue_name, body=message)
    utils.eprint("Sent: " + message + " into queue: " + queue_name)
    connection.close()


# Flask Routes


@APP.route("/")
def hello():
    utils.eprint("hello")
    return "Hello World!"


def upload_to_storage(file_path, bucketname, filename):
    return ms.upload(file_path, bucketname, filename)


@APP.route("/upload", methods=["POST"])
def handle_upload():
    utils.eprint('Received upload request..')
    # check if the post request has the file part
    if 'file' not in request.files:
        return "No file part"
    if 'room' not in request.form:
        return 'No room given'
    file = request.files['file']
    room = request.form['room']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        return "No selected file"
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        save_path = os.path.join(APP.config['UPLOAD_FOLDER'], filename)
        file.save(save_path)
        # bucketname = utils.get_bucket_name(filename)
        #key = utils.make_bucket_key(room, filename)
        key = rs.set_bucket_key(room, filename)
        utils.eprint(key)
        upload_to_storage(save_path, key, filename)
        update_status_upload(key, 'uploaded')
        send_extract_job(key, filename)
        os.remove(save_path)
        return "upload success"
    return 'File not supported'


@APP.route("/download/<room>/<uuid>", methods=["GET"])
def handle_download(room, uuid):
    utils.eprint("Received download request")
    utils.eprint(f"room: {room}, uuid: {uuid}")
    downloadable = rs.get_downloadable(room, uuid)
    downloadable = utils.to_string(downloadable)
    # utils.eprint(rs.get_bucket_key(room, uuid))
    data = ms.download(utils.make_bucket_key(room, uuid), downloadable)
    return send_file(data, attachment_filename=downloadable)


# SocketIO bd


def send_files_status(room):
    room = utils.to_string(room)
    utils.eprint('Sending files status...')
    resp = []
    redis = rs.get_redis()
    for key in redis.scan_iter(f"{room}:*:status"):
        item = {}
        utils.eprint(key)
        key = utils.to_string(key).rsplit(":", 1)[0]
        uuid = utils.to_string(key).split(':', 2)[1]
        name = redis.get(uuid + ":filename")
        name = utils.to_string(name)
        message = redis.get(key + ":status")
        message = utils.to_string(message)
        progress = utils.get_progress(message.split(' ', 1)[0])
        item['name'] = name
        item['progress'] = progress
        item['message'] = message
        item['uuid'] = uuid
        resp.append(item)
    utils.eprint('Sending: ' + str(resp))
    utils.eprint('To room: ' + room)
    SIO.emit("status", resp, room=room)


@SIO.on('connection')
def on_connection(sid):
    utils.eprint('client connected: sid = ' + sid)


@SIO.on('join room')
def join_room(sid, room):
    utils.eprint('joining ' + room + ' room')
    SIO.enter_room(sid, room)
    send_files_status(room)


@SIO.on('leave room')
def leave_room(sid, room):
    utils.eprint('leaving ' + room + ' room')
    SIO.leave_room(sid, room)
    send_files_status(room)


@SIO.on('test')
def test(sid, message):
    utils.eprint('sid: ' + sid)
    utils.eprint('message: ' + message)
    SIO.emit('test', 'reply from webapi')


def handle_update_status(body):
    # extract message
    utils.eprint(" Handle Update Status: Received %r" % body)
    if isinstance(body, bytes):
        parsed_body = utils.parser(body)
    elif isinstance(body, str):
        parsed_body = json.loads(body)
    else:
        parsed_body = body
    # json_body = json.loads(body)
    status = parsed_body.get('status')
    bucket = parsed_body.get('bucket')

    if status == 'uploaded':
        sh.handle_uploaded(bucket)
    elif status == 'extracted':
        total = parsed_body.get('total')
        targets = parsed_body.get('targets')
        sh.handle_extracted(bucket, total, targets)

    elif status == 'processed':
        object_name = parsed_body.get('object')
        sh.handle_processed(bucket)

    elif status == 'compressed':
        sh.handle_compressed(bucket)

    elif status == 'complete':
        object_name = parsed_body.get('object')
        sh.handle_complete(bucket, object_name)

    elif status == 'error':
        error = parsed_body.get('msg')
        sh.handle_error(bucket, f"error {error}")
    else:
        utils.eprint('unknown status')
        return
    room = rs.get_redis().get(f"{bucket}:room")
    send_files_status(room)


# Rabbit Receiver
class RabbitThread:
    def __init__(self, interval=1):
        self.interval = interval
        self.rabbit_url = "amqp://" + RABBIT_HOST + ":" + RABBIT_PORT + "/"
        self.conn = Connection(self.rabbit_url)
        self.exchange = Exchange("update_status", type="direct")
        self.queue = Queue(
            name="update_status",
            exchange=self.exchange,
            routing_key="update_status")
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread
        thread.start()  # Start the execution

    def run(self):
        def process_message(body, message):
            utils.eprint("The body is {}".format(body))
            handle_update_status(body)
            message.ack()

        while True:
            with Consumer(
                    self.conn,
                    queues=self.queue,
                    callbacks=[process_message],
                    accept=["json"]):
                self.conn.drain_events()


if __name__ == '__main__':
    RABBIT = RabbitThread()
    APP.run(threaded=True, host='0.0.0.0')
