import os

import redis
import shortuuid

import utils

REDIS_HOST = os.getenv('REDIS_HOST', 'localhost')
REDIS_PORT = 6379
R = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)


def make_filename_key(bucket):
    return utils.convert_key_to_regis_key(bucket) + ":filename"


def make_status_key(bucket):
    return utils.convert_key_to_regis_key(bucket) + ":status"


def make_total_key(bucket):
    return utils.convert_key_to_regis_key(bucket) + ":total"


def make_all_files_key(bucket):
    return utils.convert_key_to_regis_key(bucket) + ":all_files"


def make_p_counter_key(bucket):
    return utils.convert_key_to_regis_key(bucket) + ":p_counter"


def make_downloadable_key(bucket):
    return utils.convert_key_to_regis_key(bucket) + ":downloadable"


def make_error_key(bucket):
    return utils.convert_key_to_regis_key(bucket) + ":error"


def handle_uploaded(bucket):
    update_status(bucket, 'uploaded')


def handle_extracted(bucket, total, all_files):
    key = make_total_key(bucket)
    R.set(key, total)
    key = make_all_files_key(bucket)
    R.set(key, str(all_files))
    update_status(bucket, 'extracted')


def handle_processed(bucket):
    key = make_p_counter_key(bucket)
    total_key = make_total_key(bucket)
    pipe = R.pipeline()

    total = pipe.get(total_key)
    count = pipe.incr(key)
    total, count = pipe.execute()
    total, count = int(total or 0), int(count or 0)
    print(total, count)
    update_status(bucket, f'processed {count}/{total}')


def handle_compressed(bucket):
    update_status(bucket, 'compressed')


def handle_complete(bucket, downloadable):
    key = make_downloadable_key(bucket)
    R.set(key, downloadable)
    update_status(bucket, 'complete')


def handle_error(bucket, error):
    # key = make_error_key(bucket)
    # R.set(key, error)
    update_status(bucket, error)


def update_status(bucket, status):
    key = make_status_key(bucket)
    R.set(key, status)


def get_status(key):
    return R.get(key)


def increment_counter(key):
    return R.incr(key)


def get_downloadable(room, uuid):
    key = room + ":" + uuid + ":downloadable"
    return R.get(key)


def set_bucket_key(room, filename, uuid=None):
    if not uuid:
        uuid = shortuuid.uuid().lower()
    key = f"{room}:{filename}"
    R.set(key, uuid)
    R.set(f"{uuid}:filename", filename)
    R.set(f"{uuid}:room", room)
    return f"{uuid}"


def get_bucket_key(room, uuid):
    key = f"{room}:{uuid}"
    return R.get(key)


def get_redis():
    return R
